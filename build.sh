#!/bin/bash -x
GOPATH="$(realpath ${BASH_SOURCE[0]%/*})"

set -e

cd "$GOPATH/src"
go build
